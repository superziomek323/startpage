import timeAndDate from './timeAndDate.js';
import search from './search.js';

window.addEventListener('load', () => timeAndDate());
window.addEventListener('keydown', search);