export default function(e) {
    if(e.keyCode === 13) {
        const form = document.getElementById('search').value;
        window.location = `https://duckduckgo.com/?q=${form}`;
    }
}