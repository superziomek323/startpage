export default function timeAndDate() {
    // html elements handle
    const time = document.getElementById('time');
    const Date_ = document.getElementById('date');
    // crate new instance Date
    const date = new Date()
    // all time information
    let hour = date.getHours();
    let minute = date.getMinutes();
    if(minute < 10) minute = "0" + minute
    // all date information
    let year = date.getFullYear();
    let month = date.getMonth();
    let day = date.getDay();
    // show time and date
    time.textContent = `${hour}:${minute}`;
    Date_.textContent = `${year}/${month}/${day}`;
    // run timeAndDate function
    setTimeout(timeAndDate, 1000)
}